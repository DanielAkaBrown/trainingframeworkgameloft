#pragma once
#include "Math.h"

struct Vertex
{
	Vertex(Vector3& pos, Vector2& texCoord){
		this->pos = pos;
		this->texCoord = texCoord;
	}

	inline Vector3* GetPos() { return &pos; }
	inline Vector2* GetTexCoord() { return &texCoord; }

	Vector2 texCoord;
	Vector3 pos;
	Vector3 color;
};

class Mesh
{
public:
	//Mesh(){}
	void init(Vertex* vertices, unsigned int numVertices);

	void clean();

	void Draw(ESContext *esContext);

	virtual ~Mesh();

	GLuint vboId;
protected:
private:
	//Mesh(const Mesh& other);
	//void operator=(const Mesh& other);

	enum{
		POSITION_VB,
		TEXCOORD_VB,
		NUM_BUFFERS
	};

	GLuint m_vertexArrayBuffers[NUM_BUFFERS];
	unsigned int m_drawCount;
};