#pragma once

#include "../Utilities/utilities.h"

struct Texture{
public:
	Texture(const char* fileName);

	void Bind(unsigned int unit);

	virtual ~Texture();
	
protected:

private:
	Texture(const Texture& other);
	void operator=(const Texture& other);

	GLuint m_texture;
};