#pragma once
#include "../Utilities/utilities.h"

class Shaders
{
public:

	Shaders() {}

	virtual ~Shaders();

	int Init(char * fileVertexShader, char * fileFragmentShader);

	void Bind();

	//GLuint program, vertexShader, fragmentShader;
	char fileVS[260];
	char fileFS[260];
	GLint positionAttribute;

	GLint colorAttribute;
	GLint matrixTransform;

	GLint uvAttrib;
	GLint textureUniform;
	GLint uniformLocation;

private:
	static const unsigned int NUM_SHADERS = 2;
	Shaders(const Shaders& other) {}
	void operator=(const Shaders& other) {}

	GLuint program, vertexShader, fragmentShader;
	GLuint m_shaders[NUM_SHADERS];

};