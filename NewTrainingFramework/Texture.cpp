#include "stdafx.h"
#include "Texture.h"
//#include "std_image.h"
#include "assert.h"
#include <iostream>

Texture::Texture(const char* fileName){
	//stbi_load();

	int width, height, bpp;
	char * bufferTGA = LoadTGA(fileName, &width, &height, &bpp);

	if (bufferTGA == NULL)
		std::cerr << "Texture loading faild for texture: " << fileName << std::endl;

	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// load the image data into OpenGL ES texture resource 
	if (bpp == 24) { //bpp = bits per pixel (8*number of channels, for RGBA = 8*4=32)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, bufferTGA);
	}
	else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bufferTGA);
	}

	// free the client memory 
	//delete[] bufferTGA;

	///////////////////

	// load the image data into OpenGL ES texture resource 
	if (bpp == 24) { //bpp = bits per pixel (8*number of channels, for RGBA = 8*4=32)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, bufferTGA);
	}
	else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bufferTGA);
	}
	// free the client memory 
	delete[] bufferTGA;

	//set the filters for minification and magnification
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// generate the mipmap chain 
	glGenerateMipmap(GL_TEXTURE_2D);

	//set the wrapping modes 
	/*if (tiling == REPEAT) {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	if (tiling == CLAMP_TO_EDGE) {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}*/
	//stbi_image_free();
}

Texture::~Texture(){
	glDeleteTextures(1, &m_texture);
}

void Texture::Bind(unsigned int unit){
	assert(unit >= 0 && unit <= 31);

	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, m_texture);
}