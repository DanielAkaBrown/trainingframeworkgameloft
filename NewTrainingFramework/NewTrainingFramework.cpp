// NewTrainingFramework.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../Utilities/utilities.h" 
#include "Vertex.h"
#include "Shaders.h"
#include "Globals.h"
#include <conio.h>
#include "Texture.h"


//GLuint vboId;
//Shaders myShaders;
//
//int Init ( ESContext *esContext )
//{
//	glClearColor ( 0.0f, 0.0f, 0.0f, 0.0f );
//
//	//triangle data (heap)
//	Vertex verticesData[3];
//
//	verticesData[0].pos.x =  0.0f;  verticesData[0].pos.y =  0.5f;  verticesData[0].pos.z =  0.0f;
//	verticesData[1].pos.x = -0.5f;  verticesData[1].pos.y = -0.5f;  verticesData[1].pos.z =  0.0f;
//	verticesData[2].pos.x =  0.5f;  verticesData[2].pos.y = -0.5f;  verticesData[2].pos.z =  0.0f;
//
//	//buffer object
//	glGenBuffers(1, &vboId); //buffer object name generation
//	glBindBuffer(GL_ARRAY_BUFFER, vboId); //buffer object binding
//	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesData), verticesData, GL_STATIC_DRAW); //creation and initializion of buffer onject storage
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//
//	//creation of shaders and program 
//	return myShaders.Init("../Resources/Shaders/TriangleShaderVS.vs", "../Resources/Shaders/TriangleShaderFS.fs");
//
//}
//
//void Draw ( ESContext *esContext )
//{
//	glClear(GL_COLOR_BUFFER_BIT);
//
//	glUseProgram(myShaders.program);
//
//	glBindBuffer(GL_ARRAY_BUFFER, vboId);
//
//	
//	if(myShaders.positionAttribute != -1) //attribute passing to shader, for uniforms use glUniform1f(time, deltaT); glUniformMatrix4fv( m_pShader->matrixWVP, 1, false, (GLfloat *)&rotationMat );
//	{
//		glEnableVertexAttribArray(myShaders.positionAttribute);
//		glVertexAttribPointer(myShaders.positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
//	}
//
//	glDrawArrays(GL_TRIANGLES, 0, 3);
//
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//
//	eglSwapBuffers ( esContext->eglDisplay, esContext->eglSurface );
//}

void Update(ESContext *esContext, float deltaTime)
{

}

void Key(ESContext *esContext, unsigned char key, bool bIsPressed)
{

}

//void CleanUp()
//{
//	glDeleteBuffers(1, &vboId);
//}

int _tmain(int argc, _TCHAR* argv[])
{
	ESContext esContext;

	esInitContext(&esContext);

	esCreateWindow(&esContext, "Hello Triangle", Globals::screenWidth, Globals::screenHeight, ES_WINDOW_RGB | ES_WINDOW_DEPTH);

	/*if ( Init ( &esContext ) != 0 )
	return 0;*/

	//esRegisterDrawFunc(&esContext, Draw);
	//esRegisterUpdateFunc(&esContext, Update);
	//esRegisterKeyFunc(&esContext, Key);

	Vertex verticesData[] = { Vertex(Vector3(-0.5, -0.5, 0), Vector2(0, 0)),
		Vertex(Vector3(0, 0.5, 0), Vector2(0.5, 1.0)),
		Vertex(Vector3(0.5, -0.5, 0), Vector2(1.0, 0)) };

	Mesh mesh;
	mesh.init(verticesData, sizeof(verticesData) / sizeof(verticesData[0]));

	Shaders myShaders;
	myShaders.Init("../Resources/Shaders/TriangleShaderVS.vs", "../Resources/Shaders/TriangleShaderFS.fs");

	Texture texture("../Resources/Textures/Grass.tga");

	myShaders.Bind();
	texture.Bind(0);
	mesh.Draw(&esContext);

	esMainLoop(&esContext);

	//releasing OpenGL resources
	mesh.clean();
	//CleanUp();

	//identifying memory leaks
	MemoryDump();
	printf("Press any key...\n");
	_getch();

	return 0;
}

