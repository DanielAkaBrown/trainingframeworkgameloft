#include "stdafx.h"
#include "Shaders.h"

int Shaders::Init(char * fileVertexShader, char * fileFragmentShader)
{

	vertexShader = esLoadShader(GL_VERTEX_SHADER, fileVertexShader);

	if (vertexShader == 0)
		return -1;

	fragmentShader = esLoadShader(GL_FRAGMENT_SHADER, fileFragmentShader);

	if (fragmentShader == 0)
	{
		glDeleteShader(vertexShader);
		return -2;
	}

	program = esLoadProgram(vertexShader, fragmentShader);

	// get the uniform sampler location

	//finding location of uniforms / attributes
	glBindAttribLocation(program, 0, "a_posL");
	glBindAttribLocation(program, 1, "a_uv");
	//positionAttribute = glGetAttribLocation(program, "position");
	//colorAttribute = glGetAttribLocation(program, "texCoord");
	//matrixTransform = glGetUniformLocation(program, "u_matT");

	uniformLocation = glGetUniformLocation(program, "../Resources/Textures/Grass.tga");
	glUniform1i(uniformLocation, 0);

	return 0;
}

Shaders::~Shaders()
{
	glDeleteProgram(program);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void Shaders::Bind(){
	glUseProgram(program);
}