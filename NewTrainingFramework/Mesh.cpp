#include "stdafx.h"
#include "../Utilities/utilities.h" 
#include "Vertex.h"
#include "Shaders.h"
#include "Globals.h"
#include <conio.h>
#include <iostream>
#include <vector>

using namespace std;

void Mesh::init(Vertex* vertices, unsigned int numVertices){
	m_drawCount = numVertices;

	vector<Vector3> positions;
	vector<Vector2> texCoords;

	positions.reserve(numVertices);
	texCoords.reserve(numVertices);

	//for (unsigned int i = 0; i < numVertices; ++i){
	//	
	//	//printf("@@ %i", *vertices[i].GetPos());
	//	positions.push_back(*vertices[i].GetPos());
	//	texCoords.push_back(*vertices[i].GetTexCoord());
	//}

	positions.push_back(Vector3(0.0, 0.5, 0));
	positions.push_back(Vector3(-0.5, -0.5, 0));
	positions.push_back(Vector3(0.5, -0.5, 0));

	texCoords.push_back(Vector2(0, 0));
	texCoords.push_back(Vector2(0.5, 1.0));
	texCoords.push_back(Vector2(1.0, 0));

	//buffer object
	glGenBuffers(NUM_BUFFERS, &vboId); //buffer object name generation

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[POSITION_VB]); //buffer object binding
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(positions[0]), &positions[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);


	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[TEXCOORD_VB]); //buffer object binding
	glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(texCoords[0]), &texCoords[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//creation and initializion of buffer onject storage
	glBindBuffer(GL_ARRAY_BUFFER, 0);

}

Mesh::~Mesh(){

}

void Mesh::Draw(ESContext *esContext){

	//glClear(GL_COLOR_BUFFER_BIT);

	//glUseProgram(myShaders.program);

	glBindBuffer(GL_ARRAY_BUFFER, vboId);

	//attribute passing to shader, for uniforms use glUniform1f(time, deltaT);
	//glUniformMatrix4fv( m_pShader->matrixWVP, 1, false, (GLfloat *)&rotationMat );
	/*if (myShaders.positionAttribute != -1)
	{
	glEnableVertexAttribArray(myShaders.positionAttribute);
	glVertexAttribPointer(myShaders.positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	}*/

	glDrawArrays(GL_TRIANGLES, 0, m_drawCount);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	eglSwapBuffers(esContext->eglDisplay, esContext->eglSurface);
}

void Mesh::clean(){
	glDeleteBuffers(1, &vboId);
}