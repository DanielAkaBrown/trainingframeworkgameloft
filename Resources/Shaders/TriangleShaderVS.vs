attribute vec3 a_color;
varying vec3 v_color; 
uniform mat4 u_matT;

attribute vec2 a_uv; 
varying vec2 v_uv;
attribute vec3 a_posL; 

void main()
{
	//vec4 posL = vec4(a_posL, 1.0);
	//gl_Position = posL;

	//gl_Position = u_matT*vec4(a_posL, 1.0);

	//gl_Position = vec4(a_posL, 1.0);
	//v_color = a_color;

	gl_Position = vec4(a_posL, 1.0); 
	v_uv = a_uv;
}